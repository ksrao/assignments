#!/bin/bash
setenforce 1
sed -i 's/#PermitRootLogin yes/PermitRootLogin yes/g' /etc/ssh/sshd_config
sed -i 's/#PasswordAuthentication yes/PasswordAuthentication yes/g' /etc/ssh/sshd_config
service sshd restart
echo "secret" | passwd --stdin root

yum install wget -y



####################################################################################
install_git()
{
yum install git -y
}

#########################################################################

install_jdk()
{
#Installing Java
wget -c --header "Cookie: oraclelicense=accept-securebackup-cookie" http://download.oracle.com/otn-pub/java/jdk/8u131-b11/d54c1d3a095b4ff2b6607d096fa80163/jdk-8u131-linux-x64.rpm -P /root
rpm -ivh /root/jdk-8u131-linux-x64.rpm
}

##############################################################

install_ant()
{
wget http://ftp.cuhk.edu.hk/pub/packages/apache.org//ant/binaries/apache-ant-1.9.15-bin.tar.gz
tar -xvzf apache-ant-1.9.15-bin.tar.gz
cp -r apache-ant-1.9.15 /opt

}

################################################
install_maven()
{
#Installing Maven 
wget http://apache.communilink.net/maven/maven-3/3.6.3/binaries/apache-maven-3.6.3-bin.tar.gz
tar -xvzf apache-maven-3.6.3-bin.tar.gz
cp -r  apache-maven-3.6.3  /opt

}
############################
install_gradle()
{
wget https://services.gradle.org/distributions/gradle-6.2.1-bin.zip -P /opt
cd /opt && unzip gradle-6.2.1-bin.zip
rm -rf /opt/gradle-6.2.1-bin.zip

}

install_docker()
{
yum install docker -y

service docker start

sudo curl -L https://github.com/docker/compose/releases/download/1.21.0/docker-compose-`uname -s`-`uname -m` | sudo tee /usr/local/bin/docker-compose > /dev/null

sudo chmod +x /usr/local/bin/docker-compose

ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose

docker-compose --version
}

###########
set_env()
{
sed -i '9 a  MAVEN_HOME=/opt/apache-maven-3.6.3' ~/.bash_profile
sed -i '10 a ANT_HOME=/opt/apache-ant-1.9.15' ~/.bash_profile
sed -i '11 a GRADLE_HOME=/opt/gradle-6.2.1' ~/.bash_profile
sed -i 's/PATH=$PATH:$HOME\/bin/PATH=$PATH:$HOME\/bin:$MAVEN_HOME\/bin:$ANT_HOME\/bin:$GRADLE_HOME\/bin/g' ~/.bash_profile
source ~/.bash_profile
}


install_git
install_jdk
install_ant
install_maven
install_gradle
install_docker
set_env

