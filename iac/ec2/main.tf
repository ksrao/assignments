terraform {
  required_version = ">= 0.11.0"
}

provider "aws" {
  region = "${var.aws_region}"
}

resource "aws_instance" "amazonlinux" {
  ami           = "${var.ami_id}"
  instance_type = "${var.instance_type}"
  count = 1
  vpc_security_group_ids = [
    "sg-0d8bdc71aee9f"
  ]
  availability_zone = "${var.aws_region}"
  user_data = "${file("init.sh")}"

  tags {
    Name = "${var.name}"
  }
}
