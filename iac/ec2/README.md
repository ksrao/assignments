# Provision an EC2 instance in AWS 
This Terraform configuration provisions an EC2 instance in AWS with below tools installed.
Git Client
JDK
Apache Ant
Maven
Gradle

## Details
By default, this configuration provisions AmazonLinux  5.10 Base Image AMI (with ID ami-0e0ff68cb8e9a188a)  and with type t2.micro 
in the ap-south-1 region. 
The AMI ID, region, and type can all be set as variables. 
Note: Set below Environment varibles to authenticate 
AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY.

How To Execute:
Prerequisites
1)Terraform installed
Login to the server which terraform available
check out code from git
execute below trttaform commands
terraform init
terraform plan
terraform apply