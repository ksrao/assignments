variable "aws_region" {
  description = "AWS region"
  default = "ap-south-1"
}

variable "ami_id" {
  description = "ID of the AMI to provision. Default is Amazon linux 2 5.10 Base Image"
  default = "ami-0e0ff68cb8e9a188a"
}

variable "instance_type" {
  description = "type of EC2 instance to provision."
  default = "t2.micro"
}

variable "name" {
  description = "name to pass to Name tag"
  default = "AmazonLinux"
}
