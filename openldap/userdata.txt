#!/bin/bash
fqdn=ldap.techsys.com
setenforce 0
sed -i 's/#PermitRootLogin yes/PermitRootLogin yes/g' /etc/ssh/sshd_config
sed -i 's/#PasswordAuthentication yes/PasswordAuthentication yes/g' /etc/ssh/sshd_config
service sshd restart
echo "root" | passwd --stdin root
yum install wget git telnet -y

yum -y install openldap-clients openldap-servers
systemctl start slapd
systemctl enable slapd
systemctl status slapd
slappasswd redhat > /root/passwd
ls /etc/openldap/slapd.d/
git clone https://gitlab.com/ksrao/ldapserver.git ldapserver
cd /root/ldapserver
ldapsearch -Y EXTERNAL -H ldapi:/// -b cn=config olcDatabase=\*


ldapmodify -Y EXTERNAL -H ldapi:/// -f my_config.ldif
ldapmodify -Y EXTERNAL -H ldapi:/// -f my_config2.ldif
ldapmodify -Y EXTERNAL -H ldapi:/// -f my_config3.ldif


ldapsearch -Y EXTERNAL -H ldapi:/// -b cn=config olcDatabase=\*


slaptest -u

ldapadd -f techsys.ldif -D cn=admin,dc=techsys,dc=com -w redhat
ldapsearch -x -b dc=techsys,dc=com

ldapadd -f users.ldif -D cn=admin,dc=techsys,dc=com -w redhat
 
cp /usr/share/openldap-servers/DB_CONFIG.example /var/lib/ldap/DB_CONFIG
chown ldap:ldap /var/lib/ldap/*
ldapadd -Y EXTERNAL -H ldapi:/// -f /etc/openldap/schema/cosine.ldif
ldapadd -Y EXTERNAL -H ldapi:/// -f /etc/openldap/schema/nis.ldif
ldapadd -Y EXTERNAL -H ldapi:/// -f /etc/openldap/schema/inetorgperson.ldif
   
ldapadd -f archimedes.ldif -x -D cn=admin,dc=techsys,dc=com -w redhat
ldapadd -f testuser.ldif -x -D cn=admin,dc=techsys,dc=com -w redhat

ldappasswd -s redhat -W -D "cn=admin,dc=techsys,dc=com" -x "uid=srinivask,ou=users,dc=techsys,dc=com"

amazon-linux-extras install epel -y
yum install httpd phpldapadmin -y
cd /root/ldapserver
cp -f config.php /etc/phpldapadmin/config.php
service httpd start