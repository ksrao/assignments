#!/bin/bash
setenforce 0
sed -i 's/#PermitRootLogin yes/PermitRootLogin yes/g' /etc/ssh/sshd_config
sed -i 's/#PasswordAuthentication yes/PasswordAuthentication yes/g' /etc/ssh/sshd_config
service sshd restart
echo "root" | passwd --stdin root
sudo amazon-linux-extras install epel -y
yum -y install nagios nagios-plugins-all nagios-plugins-nrpe nrpe httpd php
chkconfig httpd on && chkconfig nagios on
service httpd start && service nagios start
#echo "nagiosadmin"|htpasswd -c /etc/nagios/passwd --stdin nagiosadmin

htpasswd -c -b  /etc/nagios/passwd nagiosadmin admin

#Updating Mail Address 
sed -i 's/nagios@localhost/kanisetty.srinivas@gmail.com/g'  /etc/nagios/objects/contacts.cfg


#Adding Linux Node for Monitoring 
cp /etc/nagios/objects/localhost.cfg /etc/nagios/objects/linuxnode.cfg


sed -i '35 a cfg_file=/etc/nagios/objects/linuxnode.cfg' /etc/nagios/nagios.cfg

#Update Ip Address of the machine 
#sed -i 's/127.0.0.1/172.31.11.134/g'  /etc/nagios/objects/linuxnode.cfg


#Replace localhost with machine which wewanted to add
#sed -i 's/localhost/linuxnode/g'  /etc/nagios/objects/linuxnode.cfg


#Delete Linux-members group

#sed 'm,nd' /etc/nagios/objects/linuxnode.cfg

htpasswd -c -b  /etc/nagios/passwd nagiosadmin admin