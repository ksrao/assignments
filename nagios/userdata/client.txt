#!/bin/bash
set enforce 0
sed -i 's/#PermitRootLogin yes/PermitRootLogin yes/g' /etc/ssh/sshd_config
sed -i 's/#PasswordAuthentication yes/PasswordAuthentication yes/g' /etc/ssh/sshd_config
service sshd restart
echo "root" | passwd --stdin root
sudo amazon-linux-extras install epel -y
yum install nrpe nagios-plugins-all -y